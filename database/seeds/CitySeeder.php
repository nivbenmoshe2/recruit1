<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            [
                'name' => 'Jerusalem',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Tel Aviv',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],        
            [
                'name' => 'Haifa',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Eilat',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],   
            [
                'name' => 'Beer Sheva',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                
            ]);    
    }
}

