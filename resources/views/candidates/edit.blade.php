@extends('layouts.app')

@section('title', 'Edit candidate')

@section('content') 
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit candidate</div>        
                    <div class="card-body">      
                        <form method = "post" action = "{{action('CandidatesController@update',$candidate->id)}}">
                        @csrf
                        @METHOD('PATCH')
                        <div class="form-group">
                            <label for = "name">Candiadte name</label>
                            <input type = "text" class="form-control" name = "name" value = {{$candidate->name}}>
                        </div>
                        <div class="form-group">
                            <label for = "age">Candiadte age</label>
                            <input type = "number" min="0" max="120" class="form-control" name = "age" value = {{$candidate->age}}>
                        </div>      
                        <div class="form-group">
                            <label for = "email">Candiadte email</label>
                            <input type = "text" class="form-control" name = "email" value = {{$candidate->email}}>
                        </div> 
                        <div class="form-group">
                            <label for="city_id">Candiadte city</label>
                                <select class="form-control" name="city_id">
                                    <option value="{{ $candidate->city->id }}">{{$candidate->city->name}}</option>
                                @foreach ($cities as $city)
                                    @if($candidate->city_id != $city->id)
                                        <option value="{{ $city->id }}">
                                            {{ $city->name }}
                                        </option>
                                    @endif
                                @endforeach
                                </select>
                        </div> 
                        <div>
                            <input type = "submit" name = "submit" value = "Update candidate">
                        </div>                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>      
@endsection

