@extends('layouts.app')

@section('title', 'Create candidate')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create candidate</div>        
                    <div class="card-body">
                        <form method = "post" action = "{{action('CandidatesController@store')}}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Candidate name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> 

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Candidate email</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="age" class="col-md-4 col-form-label text-md-right">Candidate age</label>

                            <div class="col-md-6">
                                <input id="age" type="number" min="0" max="120" class="form-control @error('age') is-invalid @enderror" name="age" value="{{ old('age') }}" required autocomplete="age">

                                @error('age')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="age" class="col-md-4 col-form-label text-md-right">Candidate City</label>
                            <div class="col-md-6">
                                <select class="form-control" name="city_id">                                                                         
                                    @foreach ($cities as $city)
                                        <option value="{{ $city->id }}"> 
                                            {{ $city->name }} 
                                        </option>
                                    @endforeach    
                                    </select>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <div>
                                    <input type = "submit" class="btn btn-primary" name = "submit" value = "Create candidate">
                                </div>
                            </div>
                        </div>

                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>     
@endsection
